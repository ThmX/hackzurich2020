#! /usr/bin/env bash

# Get the model to run on this script...

SCRIPT_DIR="$(cd "$(dirname "$0")" > /dev/null 2>&1 && pwd)"

cd $SCRIPT_DIR/frontend/corona-app && npm run serve
cd -

