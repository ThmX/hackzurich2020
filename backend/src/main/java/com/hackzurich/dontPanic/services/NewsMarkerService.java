package com.hackzurich.dontPanic.services;

import com.hackzurich.dontPanic.pojos.FullMarkerDTO;
import com.hackzurich.dontPanic.pojos.NewsMarkerDTO;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Service
public class NewsMarkerService {

  private final MongoTemplate mongoTemplate;

  public NewsMarkerService(MongoTemplate mongoTemplate) {
    this.mongoTemplate = mongoTemplate;
  }

  public List<FullMarkerDTO> findAllCountries(Instant timestamp, List<String> tags) {
    Criteria criteria = new Criteria("timestamp").lte(timestamp);

    if (!CollectionUtils.isEmpty(tags)) {
      criteria.and("details.tags").in(tags);
    }

    MatchOperation matchStage = Aggregation.match(criteria);

    GroupOperation avgSentimentGroup = group("location.country", "details.type")
        .avg("sentiment").as("avgSentiment");

    ProjectionOperation projectToMatchModel = project()
        .andExpression("_id.country").as("target")
        .andExpression("_id.type").as("type")
        .andExpression("avgSentiment").as("sentiment");

    Aggregation aggregation = newAggregation(matchStage, avgSentimentGroup, projectToMatchModel);

    AggregationResults<NewsMarkerDTO> result = mongoTemplate
        .aggregate(aggregation, "news_markers", NewsMarkerDTO.class);

    return mapToGoogleChartData(result.getMappedResults());
  }

  // we dont use it
  @Deprecated
  public List<NewsMarkerDTO> findAllRegions(String country, Instant timestamp, List<String> tags) {

    Criteria criteria = new Criteria("timestamp").lte(timestamp)
        .and("location.country").is(country);

    if (!CollectionUtils.isEmpty(tags)) {
      criteria.and("details.tags").in(tags);
    }

    MatchOperation matchStage = Aggregation.match(criteria);

    GroupOperation avgSentimentGroup = group("location.region")
        .avg("sentiment").as("avgSentiment");

    ProjectionOperation projectToMatchModel = project()
        .andExpression("_id").as("target")
        .andExpression("avgSentiment").as("sentiment");

    Aggregation aggregation = newAggregation(matchStage, avgSentimentGroup, projectToMatchModel);

    AggregationResults<NewsMarkerDTO> result = mongoTemplate
        .aggregate(aggregation, "news_markers", NewsMarkerDTO.class);

    return result.getMappedResults();
  }

  public List<FullMarkerDTO> findAllCities(String country, Instant timestamp, List<String> tags) {
    Criteria criteria = new Criteria("timestamp").lte(timestamp)
        .and("location.country").is(country);

    if (!CollectionUtils.isEmpty(tags)) {
      criteria.and("details.tags").in(tags);
    }

    MatchOperation matchStage = Aggregation.match(criteria);

    GroupOperation avgSentimentGroup = group("location.city", "details.type")
        .avg("sentiment").as("avgSentiment");

    ProjectionOperation projectToMatchModel = project()
        .andExpression("_id.city").as("target")
        .andExpression("_id.type").as("type")
        .andExpression("avgSentiment").as("sentiment");

    Aggregation aggregation = newAggregation(matchStage, avgSentimentGroup, projectToMatchModel);

    AggregationResults<NewsMarkerDTO> result = mongoTemplate
        .aggregate(aggregation, "news_markers", NewsMarkerDTO.class);

    return mapToGoogleChartData(result.getMappedResults());
  }

  private List<FullMarkerDTO> mapToGoogleChartData(List<NewsMarkerDTO> markers) {
    Map<String, FullMarkerDTO> collect = markers
        .stream()
        .filter(marker -> !Objects.isNull(marker.getTarget()))
        .collect(
            Collectors.groupingBy(
                NewsMarkerDTO::getTarget,
                Collectors.collectingAndThen(
                    Collectors.toList(),
                    FullMarkerDTO::new
                )
            )
        );
    return new ArrayList<>(collect.values());
  }
}
