package com.hackzurich.dontPanic.repositories;

import com.hackzurich.dontPanic.documents.NewsMarkerDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.Instant;
import java.util.List;

public interface NewsMarkerRepository extends MongoRepository<NewsMarkerDocument, String> {
  List<NewsMarkerDocument> findAllByTimestampLessThanEqual(Instant timestamp);
  List<NewsMarkerDocument> findAllByLocation_CountryAndTimestampLessThanEqual(String country, Instant timestamp);
  List<NewsMarkerDocument> findAllByLocation_CountryAndLocation_RegionAndTimestampLessThanEqual(
      String country,
      String region,
      Instant timestamp
  );
}
