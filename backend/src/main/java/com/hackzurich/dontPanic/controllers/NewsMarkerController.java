package com.hackzurich.dontPanic.controllers;

import com.hackzurich.dontPanic.pojos.FullMarkerDTO;
import com.hackzurich.dontPanic.pojos.NewsMarkerDTO;
import com.hackzurich.dontPanic.services.NewsMarkerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@RestController
public class NewsMarkerController {

  private final NewsMarkerService markerService;

  public NewsMarkerController(NewsMarkerService markerService) {
    this.markerService = markerService;
  }

  @GetMapping("/countries")
  public ResponseEntity<List<FullMarkerDTO>> getMarkersByCountry(
      @RequestParam(required = false) Instant timestamp,
      @RequestParam(required = false) List<String> tags
  ) {
    Instant date = Optional.ofNullable(timestamp).orElse(Instant.now());
    return ResponseEntity.ok(
        markerService.findAllCountries(date, tags)
    );
  }

  @GetMapping("/regions")
  public ResponseEntity<List<NewsMarkerDTO>> getMarkersByRegion(
      @RequestParam String country,
      @RequestParam(required = false) Instant timestamp,
      @RequestParam(required = false) List<String> tags
  ) {
    Instant date = Optional.ofNullable(timestamp).orElse(Instant.now());
    return ResponseEntity.ok(
        markerService.findAllRegions(country, date, tags)
    );
  }

  @GetMapping("/cities")
  public ResponseEntity<List<FullMarkerDTO>> getMarkersByCity(
      @RequestParam String country,
      @RequestParam(required = false) Instant timestamp,
      @RequestParam(required = false) List<String> tags
  ) {
    Instant date = Optional.ofNullable(timestamp).orElse(Instant.now());
    return ResponseEntity.ok(
        markerService.findAllCities(country, date, tags)
    );
  }
}
