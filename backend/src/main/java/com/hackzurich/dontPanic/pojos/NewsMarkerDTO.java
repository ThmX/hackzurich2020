package com.hackzurich.dontPanic.pojos;

public class NewsMarkerDTO {
  String target;
  Double sentiment;
  String type;

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public Double getSentiment() {
    return sentiment;
  }

  public void setSentiment(Double sentiment) {
    this.sentiment = sentiment;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }
}
