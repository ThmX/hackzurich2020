package com.hackzurich.dontPanic.pojos;

import java.util.List;

public class FullMarkerDTO {
  private String target;
  private Double newsSentiment;
  private Double twitterSentiment;

  public FullMarkerDTO(List<NewsMarkerDTO> markers) {
    for(NewsMarkerDTO dto : markers) {
      this.target = dto.getTarget();
      if ("News".equals(dto.getType())) {
        this.newsSentiment = dto.getSentiment();
      } else {
        this.twitterSentiment = dto.getSentiment();
      }
    }
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public Double getNewsSentiment() {
    return newsSentiment;
  }

  public void setNewsSentiment(Double newsSentiment) {
    this.newsSentiment = newsSentiment;
  }

  public Double getTwitterSentiment() {
    return twitterSentiment;
  }

  public void setTwitterSentiment(Double twitterSentiment) {
    this.twitterSentiment = twitterSentiment;
  }
}
