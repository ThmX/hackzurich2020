package com.hackzurich.dontPanic.pojos;

public class NewsLocation {
  private GPS gps;
  private String city;
  private String region;
  private String country;

  public GPS getGps() {
    return gps;
  }

  public void setGps(GPS gps) {
    this.gps = gps;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getRegion() {
    return region;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }
}
