package com.hackzurich.dontPanic.documents;

import com.hackzurich.dontPanic.pojos.NewsDetails;
import com.hackzurich.dontPanic.pojos.NewsLocation;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Document("news_markers")
public class NewsMarkerDocument {
  @Id
  private String id;

  private Instant timestamp;

  private Double sentiment;

  private NewsLocation location;

  private NewsDetails details;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Instant getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Instant timestamp) {
    this.timestamp = timestamp;
  }

  public Double getSentiment() {
    return sentiment;
  }

  public void setSentiment(Double sentiment) {
    this.sentiment = sentiment;
  }

  public NewsLocation getLocation() {
    return location;
  }

  public void setLocation(NewsLocation location) {
    this.location = location;
  }

  public NewsDetails getDetails() {
    return details;
  }

  public void setDetails(NewsDetails details) {
    this.details = details;
  }
}
