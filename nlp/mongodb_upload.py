import json
import logging

from importers import mongodb

logging.basicConfig(level=logging.INFO)

import dateutil.parser


def main():
    markers = []

    with open('markers-news.json') as fp:
        markers.extend(json.load(fp))

    with open('markers-twitter.json') as fp:
        markers.extend(json.load(fp))

    for marker in markers:
        dt = dateutil.parser.parse(marker['timestamp']['$date'])
        marker['timestamp'] = dt

    mongodb.import_all(markers)


if __name__ == '__main__':
    main()
