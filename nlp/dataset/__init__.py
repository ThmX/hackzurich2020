newspapers = [
    ('APO_output.csv', 'Luzern, Switzerland'),
    ('BAZ_output.csv', 'Basel, Switzerland'),
    ('BEO_output.csv', 'Zürich, Switzerland'),
    ('BEOL_output.csv', 'Langenthal, Switzerland'),
    ('BEOLO_output.csv', 'Langenthal, Switzerland'),
    ('BEOO_output.csv', 'Zürich, Switzerland'),
    ('BIZ_output.csv', 'Zürich, Switzerland'),
    ('BIZO_output.csv', 'Zürich, Switzerland'),
    ('BLI_output.csv', 'Zürich, Switzerland'),
    ('BLIO_output.csv', 'Zürich, Switzerland'),
    ('BOL_output.csv', 'Zürich, Switzerland'),
    ('BOLO_output.csv', 'Zürich, Switzerland'),
    ('BU_output.csv', 'Bern, Switzerland'),
    ('BZ_output.csv', 'Bern, Switzerland'),
    ('CASO_output.csv', 'Zürich, Switzerland'),
    ('ENC_output.csv', 'Zürich, Switzerland'),
    ('FUW_output.csv', 'Zürich, Switzerland'),
    ('FUWO_output.csv', 'Zürich, Switzerland'),
    ('GP_output.csv', 'Zürich, Switzerland'),
    ('GPO_output.csv', 'Zürich, Switzerland'),
    ('GSCH_output.csv', 'Zürich, Switzerland'),
    ('LAL_output.csv', 'Herrliberg, Switzerland'),
    ('LATO_output.csv', 'Langenthal, Switzerland'),
    ('LB_output.csv', 'Winterthur, Switzerland'),
    ('LINT_output.csv', 'Stäfa, Switzerland'),
    ('NAU_output.csv', 'Liebefeld, Switzerland'),
    ('NNBE_output.csv', 'Bern, Switzerland'),
    ('NNBS_output.csv', 'Basel, Switzerland'),
    ('NNBU_output.csv', 'Bern, Switzerland'),
    ('NNTA_output.csv', 'Zürich, Switzerland'),
    ('NZZ_output.csv', 'Zürich, Switzerland'),
    ('NZZB_output.csv', 'Zürich, Switzerland'),
    ('NZZF_output.csv', 'Zürich, Switzerland'),
    ('NZZM_output.csv', 'Zürich, Switzerland'),
    ('NZZO_output.csv', 'Zürich, Switzerland'),
    ('NZZS_output.csv', 'Zürich, Switzerland'),
    ('SBLI_output.csv', 'Zürich, Switzerland'),
    ('SF_output.csv', 'Zürich, Switzerland'),
    ('SHZ_output.csv', 'Zürich, Switzerland'),
    ('SHZO_output.csv', 'Zürich, Switzerland'),
    ('SI_output.csv', 'Zürich, Switzerland'),
    ('SIG_output.csv', 'Zürich, Switzerland'),
    ('SIO_output.csv', 'Zürich, Switzerland'),
    ('SIS_output.csv', 'Zürich, Switzerland'),
    ('SISP_output.csv', 'Zürich, Switzerland'),
    ('SRF_output.csv', 'Switzerland'),
    ('SRFV_output.csv', 'Switzerland'),
    ('SWII_output.csv', 'Switzerland'),
    ('TA_output.csv', 'Zürich, Switzerland'),
    ('TAM_output.csv', 'Zürich, Switzerland'),
    ('TAS_output.csv', 'Zürich, Switzerland'),
    ('TASI_output.csv', 'Thalwil, Switzerland'),
    ('TAZT_output.csv', 'Zürich, Switzerland'),
    ('TELE_output.csv', 'Switzerland'),
    ('THT_output.csv', 'Thun, Switzerland'),
    ('THTO_output.csv', 'Thun, Switzerland'),
    ('TVLL_output.csv', 'Zürich, Switzerland'),
    ('TVS_output.csv', 'Zürich, Switzerland'),
    ('WOZ_output.csv', 'Zürich, Switzerland'),
    ('ZHUL_output.csv', 'Zürich, Switzerland'),
    ('ZSZ_output.csv', 'Zürich, Switzerland'),
    ('ZWA_output.csv', 'Switzerland'),
    ('ZWAO_output.csv', 'Switzerland')
]
