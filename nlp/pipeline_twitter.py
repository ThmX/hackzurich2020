import logging
import os

from extractors.location_extractor import extract_location
from extractors.sentiment_analyzer import negativity_prediction
from extractors.tag import find_tags
from importers import disk
from readers.tweet_reader import read_tweet, tweet_path, Tweet

logging.basicConfig(level=logging.INFO)


def find_country(components):
    for component in components:
        if 'political' in component['types'] and 'country' in component['types']:
            return component['long_name']

    return None


def main():
    markers = load_markers()
    logging.info('Imported {} markers...'.format(len(markers)))
    disk.import_all(markers, 'markers-twitter.json')


def load_markers():
    tweets = read_tweet(os.path.join(tweet_path, 'tweets_geo.jsonl'))
    return process_all(tweets)


def process_all(tweets):
    markers = [try_process(tweet) for tweet in tweets]
    return list(filter(None, markers))


def try_process(tweet: Tweet):
    try:
        return process(tweet)
    except Exception as ex:
        logging.error(ex)
        return None


def process(tweet: Tweet):
    tags = find_tags(tweet.text, tags=[
        'virus',
        'corona',
        'covid',
        'social distancing',
        'pandemie'
    ])

    if not tags:
        logging.info('Skipping tag "{}"'.format(tweet.id))
        return None

    if not (5.893373 <= tweet.lng <= 10.598767):
        logging.info('Skipping lng "{}"'.format(tweet.id))
        return None

    if not (45.735872 <= tweet.lat <= 47.809223):
        logging.info('Skipping lat "{}"'.format(tweet.id))
        return None

    location = extract_location(tweet)

    sentiment = 1.0 - negativity_prediction(tweet.text)

    return {
        "id": tweet.id,
        "timestamp": {
            "$date": tweet.date
        },
        "sentiment": sentiment,
        "location": {
            "gps": {
                "lat": location.lat,
                "lng": location.lng
            },
            "city": location.city,
            "region": location.region,
            "country": location.country
        },
        "details": {
            "tags": tags,
            "type": "Tweet",
            "source": "Twitter",
            "title": ""
        }
    }


if __name__ == '__main__':
    main()
