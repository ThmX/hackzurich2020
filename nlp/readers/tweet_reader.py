import json
import logging
import os
from datetime import datetime

tweet_path = os.path.join('dataset', 'Twitter')


def read_jsonl(path):
    with open(path, encoding='utf-8') as data:
        for row in list(data):
            try:
                yield json.loads(row)
            except Exception:
                logging.error('file [{}] not found/accessible'.format(path))
                yield ""
                return


class Tweet(object):
    def __init__(self, id, date, lat, lng, text):
        self.id = id
        self.date = date
        self.lat = lat
        self.lng = lng
        self.text = text


def read_tweet(jsonl_path):
    logging.info('Parsing {}...'.format(jsonl_path))
    for idx, data in enumerate(read_jsonl(jsonl_path)):
        try:
            yield Tweet(
                id=data['id'],
                date=convert_datetime(data['created_at']),
                lng=data['coordinates']['coordinates'][0],
                lat=data['coordinates']['coordinates'][1],
                text=data['full_text'].lower()
            )
        except Exception as ex:
            logging.error("Skipping tweet at line idx")


def convert_datetime(twitter_date):
    return datetime.strptime(twitter_date, '%a %b %d %H:%M:%S +0000 %Y').isoformat()
