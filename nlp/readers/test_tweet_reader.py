import os
import unittest

from assertpy import assert_that

from readers.tweet_reader import read_tweet, tweet_path


class TestTweetReader(unittest.TestCase):
    def test_load_articles(self):
        tweets = list(read_tweet(os.path.join(tweet_path, 'tweets_geo.jsonl')))
        assert_that(tweets).is_length(10)
