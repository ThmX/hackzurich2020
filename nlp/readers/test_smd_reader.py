import os
import unittest

from assertpy import assert_that

from readers.smd_reader import read_smd, smd_path


class TestSmdReader(unittest.TestCase):
    def test_load_articles(self):
        articles = list(read_smd(os.path.join(smd_path, 'APO_output.csv')))
        assert_that(articles).is_length(116)
