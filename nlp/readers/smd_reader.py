import json
import csv
import logging
import os

from nested_lookup import nested_lookup

# Some rows exceed the default max python csv field_size_limit of 131072.
csv.field_size_limit(2147483647)

smd_path = os.path.join('dataset', 'SMD', 'data_export')


def read_csv(path):
    try:
        with open(path, encoding='utf-8') as data:
            reader = csv.DictReader(data)
            for row in reader:
                yield row
    except IOError:
        logging.error('file [{}] not found/accessible'.format(path))
        yield ""
        return
    data.close()


class Article(object):
    def __init__(self, date, source, title, text):
        self.text = text
        self.title = title
        self.source = source
        self.date = date


def read_smd(csv_path):
    logging.info('Parsing {}...'.format(csv_path))
    for idx, row in enumerate(read_csv(csv_path)):
        try:
            tx = convert_row_to_text(row)
        except Exception as e:
            logging.error("Failed to parse row #" + str(idx) + " from [" + csv_path + "]")
            continue

        yield Article(
            date=str(row["pubDateTime"]),
            source=str(row["so_txt"]),
            title=str(row["ht"]),
            text=tx,
        )


def flatten(lst):
    return [item for sublist in lst for item in sublist]


def convert_row_to_text(row):
    data = json.loads(row["tx"])
    found = nested_lookup('text', data)
    return "\n".join(found).lower()
