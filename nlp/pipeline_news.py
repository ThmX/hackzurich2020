import logging
import os

from dataset import newspapers
from extractors.location_extractor import Location, find_location
from extractors.sentiment_analyzer import negativity_prediction
from extractors.tag import find_tags
from importers import disk
from readers.smd_reader import Article, read_smd, smd_path

logging.basicConfig(level=logging.INFO)


def find_country(components):
    for component in components:
        if 'political' in component['types'] and 'country' in component['types']:
            return component['long_name']

    return None


def main():
    markers = load_markers()
    logging.info('Imported {} markers...'.format(len(markers)))
    disk.import_all(markers, 'markers-news.json')


def load_markers():
    markers = []
    for csv_path, address in newspapers:
        articles = read_smd(os.path.join(smd_path, csv_path))
        location = find_location(address)
        markers.extend(process_all(articles, location))

        logging.info('Imported {} markers...'.format(len(markers)))
        disk.import_all(markers, 'markers-news.json')

    return markers


def process_all(articles, location: Location):
    markers = []
    for article in articles:
        if len(markers) >= 50:
            break

        try:
            markers.append(process(article, location))
        except Exception as ex:
            logging.error(ex)
            return None

    return list(filter(None, markers))


def process(article: Article, location: Location):
    tags = find_tags(article.text, tags=[
        'virus',
        'corona',
        'covid',
        'social distancing',
        'pandemie'
    ])

    if not tags:
        logging.info('Skipping tag "{}"'.format(article.title))
        return None

    sentiment = 1.0 - negativity_prediction(article.text)

    return {
        "id": 0,
        "timestamp": {
            "$date": article.date
        },
        "sentiment": sentiment,
        "location": {
            "gps": {
                "lat": location.lat,
                "lng": location.lng
            },
            "city": location.city,
            "region": location.region,
            "country": location.country
        },
        "details": {
            "tags": tags,
            "type": "News",
            "source": article.source,
            "title": article.title
        }
    }


if __name__ == '__main__':
    main()
