import googlemaps

from readers.tweet_reader import Tweet

gmaps = googlemaps.Client(key='AIzaSyBGKU5bd5UKHxn3tuusb13gn3HVogcZSfk')


def find_country(data):
    for components in data:
        if 'address_components' in components:
            for component in components['address_components']:
                if 'political' in component['types'] and 'country' in component['types']:
                    return component['long_name']
    return None


def find_region(data):
    for components in data:
        if 'address_components' in components:
            for component in components['address_components']:
                if 'political' in component['types'] and 'administrative_area_level_1' in component['types']:
                    return component['long_name']
    return None


def find_district(data):
    for components in data:
        if 'address_components' in components:
            for component in components['address_components']:
                if 'political' in component['types'] and 'administrative_area_level_2' in component['types']:
                    return component['long_name']
    return None


def find_lat(data):
    for components in data:
        if 'geometry' in components:
            return components['geometry']['location']['lat']
    return None


def find_lng(data):
    for components in data:
        if 'geometry' in components:
            return components['geometry']['location']['lng']
    return None


class Location(object):
    def __init__(self, lat, lng, city, region, country):
        self.lat = lat
        self.lng = lng
        self.city = city
        self.region = region
        self.country = country


def extract_location(tweet: Tweet):
    result = gmaps.reverse_geocode(latlng=(tweet.lat, tweet.lng))
    return Location(
        lat=tweet.lat,
        lng=tweet.lng,
        city=find_district(result),
        region=find_region(result),
        country=find_country(result)
    )


def find_location(address: str):
    result = gmaps.geocode(address)
    return Location(
        lat=find_lat(result),
        lng=find_lng(result),
        city=find_district(result),
        region=find_region(result),
        country=find_country(result)
    )
