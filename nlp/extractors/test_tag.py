import unittest

from assertpy import assert_that

from extractors.tag import find_tags


class TestTagExtractor(unittest.TestCase):
    def test_all_tags_are_found(self):
        tags = find_tags("abc def ghi", tag_list=['abc', 'ab', 'def', 'khe'])
        assert_that(tags).is_equal_to(['abc', 'ab', 'def'])
