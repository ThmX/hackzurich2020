def find_tags(text, tags):
    return [t for t in tags if t in text]
