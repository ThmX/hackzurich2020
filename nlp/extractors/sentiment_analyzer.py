# Import math for sigmoid function definition
import math


def sigmoid(x):
    return 1.0 / (1 + math.exp(-x))


# AZURE PART
key = "6ea137a404cc4706b977179bac66b41e"
endpoint = "https://hackzurich-srf-ta.cognitiveservices.azure.com/"

# Authenticate the client
from azure.ai.textanalytics import TextAnalyticsClient
from azure.core.credentials import AzureKeyCredential


def authenticate_client():
    ta_credential = AzureKeyCredential(key)
    text_analytics_client = TextAnalyticsClient(
        endpoint=endpoint, credential=ta_credential)
    return text_analytics_client


client = authenticate_client()


# Sentiment analysis function (MODIFIED)
def negativity_prediction(text):
    documents = [text[0:5000]]
    response = client.analyze_sentiment(documents=documents)[0]
    theta = [0.326672, -2.948560, 2.836709, -0.269620]
    final_score = sigmoid(theta[0] + theta[1] * response.confidence_scores.positive + theta[2] * response.confidence_scores.negative + theta[3] * response.confidence_scores.neutral)
    return final_score


# EXAMPLE
sentence = "This city is bad. The streets are horrendous and everything smells terrible.";
neg = negativity_prediction(sentence)
