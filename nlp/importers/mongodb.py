from pymongo import MongoClient


def import_all(markers):
    client = MongoClient('mongodb+srv://user:localpass@cluster0.wybmf.mongodb.net/dontpanic?authSource=admin&replicaSet=atlas-l8z49q-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true')
    db = client.dontpanic

    collection = db.news_markers
    collection.drop()

    collection = db.news_markers
    collection.insert_many(markers)
