import json


def import_all(markers, filename):
    with open(filename, 'w') as fp:
        json.dump(markers, fp, indent=4)
