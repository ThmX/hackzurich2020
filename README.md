# HackZurich2020 - Don't Panic!

## Inspiration
Aren't we all tired of hearing day-in and day-out negative news?
We decided to break the cycle and bring back positivity by showing that people are full are good sentiment!

## What it does
Our service gathers and analyzes the sentiment from provided news articles from the Schweizer Mediendatenbank (SMD) and a list of pre-selected Tweets.
All of These being displayed graphically on a map by country or by city to compare the general sentiment between news and tweets.

## How we built it
Our service is comprised of two mains components running with two independent life cycles. The first part having the task to process, analyze and extract the sentiment of the articles and the tweets using Natural Language Processing (NLP) provided by the Text Analytics Cognitive Services from Microsoft Azure which will then be uploaded into MongoDB. The second part is a WebApp built with a Spring backend and a Vue.js frontend able to querying and aggregate the data to plot the general sentiment of the news and tweets on a Map built with Google Charts.

## Challenges we ran into
- Find a meaningful and innovative way to represent the sentiment between two types of media (article vs tweet)
- Figure out the impact of the newspaper at a specific location

## Accomplishments that we are proud of
Bringing a fully working prototype while successfully integrating multitude of technologies in such a short time.

## What we learned
- Microsoft Azure Text Analytics Cognitive Services
- Google GeoLocation Service with Google Charts

## What's next for Don't Panic
- Include new news outlets in other countries and continents
- Improve the visualization of sentiment over time
- Visualize the impact of the media on the tweets
