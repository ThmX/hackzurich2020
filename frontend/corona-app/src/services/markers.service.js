import axios from 'axios';

const BASE_URL = 'https://dont-panic-be.herokuapp.com';
function getMarkersByCountry(tags, ISOtimeString) {
    const url = `${BASE_URL}/countries`;
    const _tags = tags.join(',');
    return axios.get(url, {
        params: {
            timestamp: ISOtimeString,
            tags: _tags
        }
    });
}

function getMarkersByCity(country, tags, ISOtimeString) {
    const url = `${BASE_URL}/cities`;
    const _tags = tags.join(',');
    return axios.get(url, {
        params: {
            timestamp: ISOtimeString,
            country,
            tags: _tags
        }
    });
}

export default {
    getMarkersByCountry,
    getMarkersByCity
}
