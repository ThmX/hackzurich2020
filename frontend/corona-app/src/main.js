import Vue from 'vue'
import App from './App.vue'
import router from './router'

import VueGoogleCharts from 'vue-google-charts'
import vuetify from './plugins/vuetify';


Vue.use(VueGoogleCharts)

Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
